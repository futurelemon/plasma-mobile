# Translation of kcm_mobile_virtualkeyboard.po to Catalan (Valencian)
# Copyright (C) 2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-22 01:46+0000\n"
"PO-Revision-Date: 2023-09-22 10:35+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/languages.qml:20
#, kde-format
msgid "Languages"
msgstr "Idiomes"

#: ui/languages.qml:46
#, kde-format
msgid "Apply"
msgstr "Aplica"

#: ui/main.qml:20
#, kde-format
msgid "On-Screen Keyboard"
msgstr "Teclat en pantalla"

#: ui/main.qml:35
#, kde-format
msgid "Type anything here…"
msgstr "Teclegeu ací qualsevol cosa…"

#: ui/main.qml:40
#, kde-format
msgctxt "@title:group"
msgid "Feedback"
msgstr "Retroalimentació"

#: ui/main.qml:46
#, kde-format
msgid "Sound"
msgstr "So"

#: ui/main.qml:47
#, kde-format
msgid "Whether to emit a sound on keypress."
msgstr "Si s'ha d'emetre un so en prémer les tecles."

#: ui/main.qml:56
#, kde-format
msgid "Vibration"
msgstr "Vibració"

#: ui/main.qml:57
#, kde-format
msgid "Whether to vibrate on keypress."
msgstr "Si ha de vibrar en prémer les tecles."

#: ui/main.qml:64
#, kde-format
msgctxt "@title:group"
msgid "Text Correction"
msgstr "Correcció de text"

#: ui/main.qml:71
#, kde-format
msgid "Check spelling of entered text"
msgstr "Verificació ortogràfica del text introduït"

#: ui/main.qml:80
#, kde-format
msgid "Capitalize the first letter of each sentence"
msgstr "Posa en majúscula la primera lletra de cada frase"

#: ui/main.qml:89
#, kde-format
msgid "Complete current word with first suggestion when hitting space"
msgstr "Completa la paraula actual amb el primer suggeriment en prémer espai"

#: ui/main.qml:98
#, kde-format
msgid "Suggest potential words in word ribbon"
msgstr "Suggerix paraules potencials a la franja de paraules"

#: ui/main.qml:109
#, kde-format
msgid "Insert a full-stop when space is pressed twice"
msgstr "Inserix un punt quan es prema dues vegades l'espai"

#: ui/main.qml:120
#, kde-format
msgid "Configure Languages"
msgstr "Configura els idiomes"
