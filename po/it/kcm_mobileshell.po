# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
# SPDX-FileCopyrightText: 2022, 2023 Paolo Zamponi <feus73@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-23 01:46+0000\n"
"PO-Revision-Date: 2023-09-25 21:56+0200\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.1\n"

#: ui/main.qml:18
#, kde-format
msgid "Shell"
msgstr "Shell"

#: ui/main.qml:27
#, kde-format
msgid "General"
msgstr "Generale"

#: ui/main.qml:33 ui/VibrationForm.qml:18 ui/VibrationForm.qml:25
#, kde-format
msgid "Shell Vibrations"
msgstr "Vibrazione della shell"

#: ui/main.qml:41
#, kde-format
msgid "Animations"
msgstr "Animazioni"

#: ui/main.qml:42
#, kde-format
msgid "If this is off, animations will be reduced as much as possible."
msgstr "Se non è attivata, le animazioni verranno ridotte al minimo."

#: ui/main.qml:53
#, kde-format
msgid "Navigation Panel"
msgstr "Pannello di navigazione"

#: ui/main.qml:59
#, kde-format
msgid "Gesture-only Mode"
msgstr "Modalità solo gesti"

#: ui/main.qml:60
#, kde-format
msgid "Whether to hide the navigation panel."
msgstr "Se nascondere il pannello di navigazione."

#: ui/main.qml:71
#, kde-format
msgid "Task Switcher"
msgstr "Selettore delle attività"

#: ui/main.qml:76
#, kde-format
msgid "Show Application Previews"
msgstr "Mostra le anteprime delle applicazioni"

#: ui/main.qml:77
#, kde-format
msgid "Turning this off may help improve performance."
msgstr ""
"La sua disattivazione potrebbe aiutare il miglioramento delle prestazioni."

#: ui/main.qml:88
#, kde-format
msgid "Action Drawer"
msgstr "Cassetto delle azioni"

#: ui/main.qml:94
#, kde-format
msgctxt "Pinned action drawer mode"
msgid "Pinned Mode"
msgstr "Modalità appuntata"

#: ui/main.qml:95
#, kde-format
msgctxt "Expanded action drawer mode"
msgid "Expanded Mode"
msgstr "Modalità espansa"

#: ui/main.qml:99 ui/QuickSettingsForm.qml:18 ui/QuickSettingsForm.qml:71
#, kde-format
msgid "Quick Settings"
msgstr "Impostazioni rapide"

#: ui/main.qml:107
#, kde-format
msgid "Top Left Drawer Mode"
msgstr "Modalità cassetto in alto a sinistra"

#: ui/main.qml:108
#, kde-format
msgid "Mode when opening from the top left."
msgstr "Modalità quando si apre dall'alto a sinistra."

#: ui/main.qml:133
#, kde-format
msgid "Top Right Drawer Mode"
msgstr "Modalità cassetto in alto a destra"

#: ui/main.qml:134
#, kde-format
msgid "Mode when opening from the top right."
msgstr "Modalità quando si apre dall'alto a destra."

#: ui/QuickSettingsForm.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Hide"
msgstr "Nascondi"

#: ui/QuickSettingsForm.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Show"
msgstr "Mostra"

#: ui/QuickSettingsForm.qml:76
#, kde-format
msgid ""
"Customize the order of quick settings in the pull-down panel and hide them."
msgstr ""
"Personalizza l'ordine delle impostazioni rapide nel pannello a discesa e "
"nascondile."

#: ui/QuickSettingsForm.qml:99
#, kde-format
msgid "Disabled Quick Settings"
msgstr "Impostazioni rapide disabilitate"

#: ui/QuickSettingsForm.qml:104
#, kde-format
msgid "Re-enable previously disabled quick settings."
msgstr "Abilita di nuovo le impostazioni rapide disabilitate in precedenza."

#: ui/VibrationForm.qml:26
#, kde-format
msgid "Whether to have vibrations enabled in the shell."
msgstr "Se avere attivata la vibrazione nella shell."

#: ui/VibrationForm.qml:39
#, kde-format
msgid "Vibration Intensity"
msgstr "Intensità della vibrazione"

#: ui/VibrationForm.qml:40
#, kde-format
msgid "How intense shell vibrations should be."
msgstr "Quanto dovrebbe essere intensa la vibrazione della shell."

#: ui/VibrationForm.qml:42
#, kde-format
msgctxt "Low intensity"
msgid "Low"
msgstr "Bassa"

#: ui/VibrationForm.qml:43
#, kde-format
msgctxt "Medium intensity"
msgid "Medium"
msgstr "Media"

#: ui/VibrationForm.qml:44
#, kde-format
msgctxt "High intensity"
msgid "High"
msgstr "Alta"

#: ui/VibrationForm.qml:70
#, kde-format
msgid "Vibration Duration"
msgstr "Durata vibrazione"

#: ui/VibrationForm.qml:71
#, kde-format
msgid "How long shell vibrations should be."
msgstr "Quanto dovrebbe essere lunga la vibrazione della shell."

#: ui/VibrationForm.qml:73
#, kde-format
msgctxt "Long duration"
msgid "Long"
msgstr "Lunga"

#: ui/VibrationForm.qml:74
#, kde-format
msgctxt "Medium duration"
msgid "Medium"
msgstr "Media"

#: ui/VibrationForm.qml:75
#, kde-format
msgctxt "Short duration"
msgid "Short"
msgstr "Breve"

#: ui/VibrationForm.qml:99
#, kde-format
msgid ""
"Keyboard vibrations are controlled separately in the keyboard settings "
"module."
msgstr ""
"La vibrazione della tastiera è controllata separatamente nel modulo delle "
"impostazioni della tastiera."
