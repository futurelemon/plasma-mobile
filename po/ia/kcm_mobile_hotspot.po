# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-22 01:46+0000\n"
"PO-Revision-Date: 2023-03-16 11:15+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.1\n"

#: ui/main.qml:40
#, kde-format
msgid "Hotspot"
msgstr "Hotspot"

#: ui/main.qml:41
#, kde-format
msgid "Whether the wireless hotspot is enabled."
msgstr "Si le hotspot wireless (sin cablo) es habilitate."

#: ui/main.qml:60
#, kde-format
msgid "Hotspot SSID"
msgstr "SSID de Hotspot"

#: ui/main.qml:69
#, kde-format
msgid "Hotspot Password"
msgstr "Contrasigno de Hotspot"

#~ msgid "Enabled:"
#~ msgstr "Habilitate:"

#~ msgid "SSID:"
#~ msgstr "SSID:"

#~ msgid "Save"
#~ msgstr "Salveguarda"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "Tobias Fella"
#~ msgstr "Tobias Fella"
