# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Martin Schlander <mschlander@opensuse.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-22 01:46+0000\n"
"PO-Revision-Date: 2020-07-22 20:31+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: ui/main.qml:40
#, kde-format
msgid "Hotspot"
msgstr "Internetdeling"

#: ui/main.qml:41
#, kde-format
msgid "Whether the wireless hotspot is enabled."
msgstr ""

#: ui/main.qml:60
#, fuzzy, kde-format
#| msgid "Hotspot"
msgid "Hotspot SSID"
msgstr "Internetdeling"

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Password:"
msgid "Hotspot Password"
msgstr "Adgangskode:"

#~ msgid "Enabled:"
#~ msgstr "Aktiveret:"

#~ msgid "SSID:"
#~ msgstr "SSID:"

#~ msgid "Save"
#~ msgstr "Gem"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Martin Schlander"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mschlander@opensuse.org"

#~ msgid "Tobias Fella"
#~ msgstr "Tobias Fella"
